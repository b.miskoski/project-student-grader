<?php 

if (isset($_GET['id']) && !empty(trim($_GET['id']))) {

    require_once 'config.php';

    // Prepare SQL
    $sql = "SELECT * FROM students WHERE id = :id";

    if ($stmt = $pdo->prepare($sql)) {
        
        $stmt->bindParam(":id", $param_id);

        $param_id = trim($_GET['id']);

        if ($stmt->execute()) {
                if($stmt->rowCount() == 1) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $id = $row['id'];
                    $name = $row['name'];
                    $course = $row['course'];
                    $grade = $row['grade'];
                } else {
                    header('Location: error.php');
                }
        } else {
            echo 'Something went wrong';
        }
        unset($stmt);
        unset($pdo);
    } else {
        header('Location: error.php');
    }

    $jsonData = array('id' => $id, 'name' => $name, 'course' => $course, 'grade' => $grade);

    echo json_encode($jsonData);

    
    
    
}



?>