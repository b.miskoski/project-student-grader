<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';

    $course_name_err = $course_desc_err = '';
    $course_name = $course_desc = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST' ) {

        if(empty($_POST['course_name'])) {
            $course_name_err = 'Please enter course!';
        } else {
            $course_name = filter_var($_POST['course_name'], FILTER_SANITIZE_STRING);
        }

        if(empty($_POST['course_description'])) {
            $course_desc_err = 'Please enter course description!';
        } else {
            $course_desc = $_POST['course_description'];
        }

        if(empty($course_name_err) && empty($course_desc_err)) {

            $sql = 'INSERT INTO courses (course_name, course_description, user_id) VALUES (:course_name, :course_description, :user_id)';

            if($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(':course_name', $param_course_name);
                $stmt->bindParam(':course_description', $param_course_desc);
                $stmt->bindParam(':user_id', $param_user_id);

                $param_course_name = htmlspecialchars($course_name);
                $param_course_desc = htmlspecialchars($course_desc);
                $param_user_id = trim($_SESSION['id']);
            }

            if($stmt->execute()) {
                $_SESSION['message'] = 'You have successfully created a course';
                header('Location: menage-courses.php');
                exit();
            } else {
                echo 'Something went wrong';
            }
            unset($stmt);
        }
        unset($pdo);
    }
?>

<?php require_once 'site/header.php'; ?>
    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Create Course</h2>
                </div>
                <div class="py-4">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($course_name_err)) ? 'has-error' : ''; ?>">
                            <label>Course Name</label>
                            <input type="text" name="course_name" class="form-control" value="<?php echo $course_name; ?>">
                            <span class="help-block"><?php echo $course_name_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($course_desc_err)) ? 'has-error' : ''; ?>">
                            <label for="course">Course Description</label>
                            <input type="text" name="course_description" class="form-control" value="<?php echo $course_desc; ?>">
                            <span class="help-block"><?php echo $course_desc_err; ?></span>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="menage-courses.php" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>