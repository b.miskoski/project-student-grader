<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';   
    
    $name = $course_id = $grade = '';
    $name_err = $course_id_err = $grade_err = '';

    if($_SERVER['REQUEST_METHOD'] == 'POST') {
        try {
            $input_name = trim($_POST['name']);
            if (empty($input_name)) {
                throw new Exception('Please fill out the student name');
            } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
                throw new Exception('Please enter valid name');
            } else {
                $name = $input_name;
            }
        } catch (Exception $e) {
            $name_err = $e->getMessage();
        }

        /*
        $input_name = trim($_POST['name']);
        if (empty($input_name)) {
            $name_err = 'Please fill out the student name';
        } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
            $name_err = 'Please enter valid name';
        } else {
            $name = $input_name;
        } 
        */
        if (empty(trim($_POST['course']))) {
            $course_id_err = 'Please enter course';
        } else {
            $course_id = $_POST['course'];
        }

        if (empty(trim($_POST['grade']))) {
            $grade_err = 'Please enter grade';
        } elseif (!ctype_digit($_POST['grade'])) {
            $grade_err = 'Please enter a numeric grade value';
        } elseif (intval($_POST['grade']) > 6) {
            $grade_err = 'Please enter a numeric grade from 1 to 5';
        } else {
            $grade = $_POST['grade'];
        }

        if(empty($name_err) && empty($course_id_err) && empty($grade_err)) {
            $sql = "INSERT INTO students (name, course_id, grade, user_id) VALUES (:name, :course_id, :grade, :user_id)";

            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":name", $param_name);
                $stmt->bindParam(":course_id", $param_course_id);
                $stmt->bindParam(":grade", $param_grade);
                $stmt->bindParam(':user_id', $param_user_id);

                $param_name = $name;
                $param_course_id = $course_id;
                $param_grade = $grade;
                $param_user_id = $_SESSION['id'];
            }

            if($stmt->execute()) {
                $_SESSION['message'] = 'You have successfully created a record';
                header('Location: index.php');
                exit();
            } else {
                echo "Something went wrong";
            }
            unset($stmt);
        }
    }

    
?>

<?php require_once 'site/header.php'; ?>

<?php $sql = 'SELECT * FROM courses'; ?>

    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Create Record</h2>
                </div>
                <div class="py-4">
                    <p>Please fill this form and submit to add student to the database</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                            <span class="help-block"><?php echo $name_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($course_id_err)) ? 'has-error' : ''; ?>">
                            <label for="course">Course</label>
                            <select name="course" class="form-control" id="course">
                                <option selected value="">Choose a course</option>
                                <?php if($result = $pdo->query($sql)): ?>
                                    <?php if($result->rowCount() > 0): ?>    
                                        <?php while($row = $result->fetch()): ?>
                                            <option value="<?= $row['c_id']; ?>"><?= $row['course_name'] ?></option>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                            </select>
                            <span class="help-block"><?php echo $course_id_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($grade_err)) ? 'has-error' : ''; ?>">
                            <label for="grade">Grade</label>
                            <select name="grade" class="form-control" id="grade">
                                <option selected value="">Choose a grade</option>
                                <?php for($i = 1; $i < 6; $i++) :?>
                                    <option <?= ($grade == $i) ? 'selected' : ''; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                <?php endfor; ?>
                                <?php unset($pdo); ?>
                            </select>
                            <span class="help-block"><?php echo $grade_err; ?></span>
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="index.php" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>