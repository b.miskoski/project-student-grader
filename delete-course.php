<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    if (isset($_POST['id']) && !empty($_POST['id'])) {

        require_once 'config.php';

        $sql = "DELETE FROM courses WHERE c_id = :id";

        if ($stmt = $pdo->prepare($sql)) {
            $stmt->bindParam(":id", $param_id);

            $param_id = trim($_POST['id']);

            try {
                if($stmt->execute()) {
                    throw new Exception('You have successfully deleted a course');
                }
            } catch (PDOException $e) {
                $_SESSION['error-message'] = 'You can not delete a course that have active listeners!';
                header('Location: menage-courses.php');
            } catch (Exception $e) {
                $_SESSION['message'] = $e->getMessage();
                header('Location: menage-courses.php');
            }
        }
        unset($stmt);
        unset($pdo);
    } else {
        if(empty(trim($_GET['id']))) {
            header('Location: error.php');
            exit();
        }
    }
?>

<?php require_once 'site/header.php'; ?>
    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Delete Course</h2>
                </div>
                <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                    <div class="alert alert-danger py-4" role="alert">
                        <input type="hidden" name="id" value="<?php echo trim($_GET['id']); ?>">
                        <p>Are you sure you want to delete this course?</p>
                        <input type="submit" value="Yes" class="btn btn-danger">
                        <a href="menage-courses.php" class="btn btn-light">No</a>
                    </div>
                </form>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>