<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';
?>

<?php require_once 'site/header.php'; ?>

    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="d-flex justify-content-between mb-4 border-bottom">
                    <h2>Instructor Detalis</h2>
                    <a href="reset-password.php" class="btn btn-success mr-2 mb-3">Reset Your Password</a>
                </div>

                <?php
                    $sql = "SELECT * FROM users";
                    $sql2 = 'SELECT * FROM courses';
                    $i = 1;
                ?>

                <table class="table table-striped">
                    <tr>
                        <th scope="col">#</th>
                        <th scope="col">Name</th>
                        <th scope="col">Courses</th>
                        <th scope="col">Action</th>
                    </tr>
                    <?php if($result = $pdo->query($sql)): ?>
                        <?php if($result->rowCount() > 0): ?>    
                            <?php while($row = $result->fetch()): ?>
                    <tr>
                        <td><?php echo $i; ?></td>
                        <td><?php echo $row['name'] . ' ' . $row['lastName'] ?></td>
                        <?php if($result2 = $pdo->query($sql2)): ?>
                            <td>
                            <?php if($result2->rowCount() > 0): ?>   
                                <?php 
                                    while($row2 = $result2->fetch()) { 
                                        echo $row2['course_name'] . ', ';
                                    }
                                ?>
                            </td>
                            <?php endif; ?>
                        <?php endif; ?>
                        <td>
                            <a href="instructor-details-edit.php?id=<?= $row['id']; ?>">
                                <i class="fas fa-pencil-alt mr-3 text-primary"></i>
                            </a>
                        </td>
                    </tr>   
                                    <?php $i++; ?>
                                <?php endwhile; ?>
                            <?php endif; ?>
                            <?php unset($result); ?>
                        <?php endif; ?>
                        <?php unset($pdo); ?>
                </table>
            </div>
        </div>
    </section>

<?php require_once 'site/footer.php'; ?>