<?php require_once 'site/header.php'; ?>
    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">View Record</h2>
                </div>
                <div class="py-4">
                    <div>
                        <p class="font-weight-bold">Invalid Request</p>
                        <p>Sorry your request is invalid</p>
                    </div>
                    <a href="index.php" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>