<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }
?>

<?php require_once 'site/header.php'; ?>
<?php require_once 'config.php'; ?>

    <section class="content">
        <div class="row justify-content-center mt-5">
            <div class="col-8 custom-message d-flex justify-content-between <?= (isset($_SESSION['message'])) ? 'alert alert-success' : ''; ?>">
                <?= (isset($_SESSION['message'])) ? '<p class="mb-0">' . $_SESSION['message'] . '</p><i class="close-btn align-self-center fas fa-times"></i>' : ''; ?>
            </div>
            <?php unset($_SESSION['message']); ?>
        </div>
        <div class="row my-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="d-flex justify-content-between mb-4 border-bottom">
                    <h2>Student Detalis</h2>
                    <a href="create-record.php" class="btn btn-success mb-3">Add new Student</a>
                </div>
                <div>
                    <?php
                        $sql = "SELECT * FROM students
                        INNER JOIN courses ON students.course_id = courses.c_id";
                        $i = 1;
                    ?>
                    <table class="table table-striped">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Course</th>
                            <th scope="col">Grade</th>
                            <th scope="col">Action</th>
                        </tr>
                            <?php if($result = $pdo->query($sql)): ?>
                                <?php if($result->rowCount() > 0): ?>    
                                    <?php while($row = $result->fetch()): ?>
                        <tr>
                            <td><?php echo $i; ?></td>
                            <td><?php echo $row['name']; ?></td>
                            <td><?php echo $row['course_name']; ?></td>
                            <td><?php echo $row['grade']; ?></td>
                            <td>
                                <a href="view-record.php?id=<?=$row['s_id']?>">
                                    <i class="fas fa-eye mr-3 text-primary"></i>
                                </a>
                                <a href="update-record.php?id=<?=$row['s_id']?>">
                                    <i class="fas fa-pencil-alt mr-3 text-primary"></i>
                                </a>
                                <a href="delete-record.php?id=<?=$row['s_id']?>">
                                    <i class="far fa-trash-alt text-primary"></i>
                                </a>
                            </td>
                        </tr>   
                                <?php $i++; ?>
                                    <?php endwhile; ?>
                            <?php endif; ?>
                        <?php unset($result); ?>
                        <?php endif; ?>
                        <?php unset($pdo); ?>
                    </table>
                </div>
            </div>
            <div class="col-2"></div>
        </div>
    </section>

<?php require_once 'site/footer.php'; ?>

<script>
    let messageDiv = document.getElementsByClassName('custom-message')[0];
    let closeBtn = document.getElementsByClassName('close-btn')[0];
    
    closeBtn.addEventListener('click', () => {
        messageDiv.classList.add('hidden');
    })

    setTimeout(function() {
        messageDiv.classList.add('hidden');
    },7000);
</script>