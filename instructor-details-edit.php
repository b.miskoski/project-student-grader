<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';

    $username_err = $user_email_err = '';
    $username = $user_email = '';

    if (isset($_POST['id']) && !empty($_POST['id'])) {
        
        $id = $_POST['id'];

        $input_username = trim($_POST['username']);
        if (empty($input_username)) {
            $username_err = 'Please fill out name field';
        } elseif (!filter_var($input_username, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
            $username_err = 'Please enter valid name';
        } else {
            $username = $input_username;
        }

        $input_user_email = trim($_POST['user_email']);
        if (empty(trim($input_user_email))) {
            $user_email_err = 'Please enter email';
        } elseif (filter_var($input_user_email, FILTER_VALIDATE_EMAIL) == false) {  
            $user_email_err = 'Your email addres is not valid';
        } else {
            $user_email = trim($_POST['user_email']);
        }

        if(empty($username_err) && empty($user_email_err)) {

            $sql = "UPDATE users SET name=:name, email=:email WHERE id=:id";

            if($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(':name', $param_username);
                $stmt->bindParam(':email', $param_user_email);
                $stmt->bindParam(':id', $param_user_id);

                $param_username = $username;
                $param_user_email = $user_email;
                $param_user_id = $id;

                if($stmt->execute()) {
                    header('Location: edit-profile.php');
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);
        }
        unset($pdo);
    } else {

        if(isset($_GET['id']) && !empty(trim($_GET['id']))) {
            
            $id = trim($_GET['id']);
            $sql = 'SELECT * FROM users WHERE id = :id';

            if($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(':id', $param_user_id);

                $param_user_id = $id;

                if($stmt->execute()) {
                    if ($stmt->rowCount() == 1) {
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        //var_dump($row);die;
                        $username = $row['name'];
                        $user_email = $row['email'];
                    } else {
                        header('Location: error.php');
                        exit();
                    }
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);
            unset($pdo);
        } else {
            header('Location: error.php');
            exit();
        }
    }

?>

<?php require_once 'site/header.php'; ?>

    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Update Instructor Details</h2>
                </div>
                <div class="py-4">
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($username_err)) ? 'has-error' : ''; ?>">
                            <label>Name</label>
                            <input type="text" name="username" class="form-control" value="<?php echo $username; ?>">
                            <span class="help-block"><?php echo $username_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($user_email_err)) ? 'has-error' : ''; ?>">
                            <label>Email</label>
                            <input type="email" name="user_email" class="form-control" value="<?php echo $user_email; ?>">
                            <span class="help-block"><?php echo $user_email_err; ?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="edit-profile.php" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php require_once 'site/footer.php'; ?>