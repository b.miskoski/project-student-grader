<?php

    require_once 'config.php';

    if(isset($_REQUEST['term'])) {
        
        $sql = 'SELECT * FROM students WHERE name LIKE :term';

        $stmt = $pdo->prepare($sql);
        $term = '%' . $_REQUEST['term'] . '%';

        $stmt->bindParam(':term', $param_term);

        $param_term = $term;

        $stmt->execute();

        if($stmt->rowCount() > 0) {
            while($row = $stmt->fetch()) {
                echo '<p class="mb-1"><a href="view-record.php?id='. $row['s_id'] .'">' . $row['name'] . '</a></p>';
            }
        } else {
            echo '<p>No students found</p>';
        }
    }

    unset($stmt);
    unset($pdo);