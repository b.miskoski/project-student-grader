<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://kit.fontawesome.com/a076d05399.js"></script>
        <link rel="stylesheet" href="css/style.css">
        <title>Student Grader</title>
    </head>
    <body>

        <header class="bg-success">
                <nav class="px-5">
                    <div class="row">
                        <div class="col-6">
                            <a href="/" class="navbar-brand">
                                <h1 class="text-white text-uppercase font-weight-bold">Student Grader</h1>
                            </a>
                        </div>
                        <div class="col-6 d-flex align-items-center justify-content-end collapse navbar-collapse">
                            <?php if(isset($_SESSION['loggedin']) &&  $_SESSION['loggedin'] === true): ?>
                                <div class="search-box mr-4">
                                    <input class="form-control" type="text" autocomplete="off" placeholder="Search student">
                                    <div class="result"></div>
                                </div>
                                <!-- <a href="reset-password.php" class="btn btn-warning mr-2">Reset Your Password</a> -->
                                <li class="nav-item dropdown list-unstyled">
                                    <a class="nav-link dropdown-toggle btn btn-light" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        My Account
                                    </a>
                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item <?= (basename($_SERVER['SCRIPT_FILENAME']) === 'edit-profile.php') ? 'link-active' : ''; ?>" href="edit-profile.php">Edit Profile</a>
                                        <a class="dropdown-item <?= (basename($_SERVER['SCRIPT_FILENAME']) === 'menage-courses.php') ? 'link-active' : ''; ?>" href="menage-courses.php">Menage Courses</a>
                                        <a class="dropdown-item <?= (basename($_SERVER['SCRIPT_FILENAME']) === 'index.php') ? 'link-active' : ''; ?>" href="index.php">Menage Students</a>
                                        <a class="dropdown-item" href="logout.php">Log Out <?php //if(isset($_SESSION['username'])) {echo $_SESSION['username'];} ?></a>
                                    </div>
                                </li>
                            <?php endif; ?>
                        </div>
                    </div>
                </nav>
        </header>
        
        <script>
            $(document).ready(function() {

                $('.search-box input[type="text"]').on('keyup input', function(){
                    let inputVal = $(this).val();
                    let resultDropdown = $(this).siblings('.result');

                    resultDropdown.css('display', 'block');

                    if(inputVal.length) {
                        $.get('search-students.php', 
                            {term: inputVal}).done(function(data) {
                            resultDropdown.html(data);
                        });
                    } else {
                        resultDropdown.empty();
                        resultDropdown.css('display', 'none');
                    }
                });

                $(document).on('click', '.result p', function(){
                    $(this).parents('.search-box').find('input[type="text"]').val($(this).text());
                    $(this).parent('.result').empty();
                });

            });
        </script>