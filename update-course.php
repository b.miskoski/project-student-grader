<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    require_once 'config.php';

    $course_name = $course_description = '';
    $course_name_err = $course_description_err = '';

    if (isset($_POST['id']) && !empty($_POST['id'])) {

        $id = $_POST['id'];

        // Validation course name
        $input_course_name = trim($_POST['course_name']);
        if (empty($input_course_name)) {
            $course_name_err = 'Please fill out the course name';
        } elseif (!filter_var($input_course_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
            $course_name_err = 'Please enter valid name';
        } else {
            $course_name = $input_course_name;
        }

        // Validation course name
        $input_course_description = trim($_POST['course_description']);
        if (empty($input_course_description)) {
            $course_description_err = 'Please fill out the course description';
        } else {
            $course_description = $input_course_description;
        }

        if (empty($course_name_err) && empty($course_description_err)) {
            
            $sql = "UPDATE courses SET course_name=:course_name, course_description=:course_description WHERE c_id=:id";

            // Prepare
            if ($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(":course_name", $param_course_name);
                $stmt->bindParam(":course_description", $param_course_description);
                $stmt->bindParam(":id", $param_id);

                $param_course_name = $course_name;
                $param_course_description = $course_description;
                $param_id = $id;

                if($stmt->execute()) {
                    $_SESSION['message'] = 'You have successfully updated a course';
                    header('Location: menage-courses.php');
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);

        }
        unset($pdo);
    } else {
        // Showing input values logic goes here
        if(isset($_GET['id']) && !empty(trim($_GET['id']))) {

            // We get here the url parametar in a variable
            $id = trim($_GET['id']);

            // Sql statement
            //$sql = "SELECT * FROM students WHERE id = :id";
            $sql = "SELECT * FROM courses WHERE c_id = :id";

            // Prepare statement
            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":id", $param_id);

                // Set param id
                $param_id = $id;

                // Execute
                if ($stmt->execute()) {
                    if ($stmt->rowCount() == 1) {
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        $course_name = $row['course_name'];
                        $course_description = $row['course_description'];

                    } else {
                        // There is no valid id param
                        header('Location: error.php');
                        exit();
                    }
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);
            unset($pdo);
        } else {
            header('Location: error.php');
            exit();
        }
    }
?>

<?php require_once 'site/header.php'; ?>
    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Update Course</h2>
                </div>
                <div class="py-4">
                    <p>Please edit the input values and submit to update the course name and description</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($course_name_err)) ? 'has-error' : ''; ?>">
                            <label>Course Name</label>
                            <input type="text" name="course_name" class="form-control" value="<?php echo $course_name; ?>">
                            <span class="help-block"><?php echo $course_name_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($course_description_err)) ? 'has-error' : ''; ?>">
                            <label>Course Description</label>
                            <input type="text" name="course_description" class="form-control" value="<?php echo $course_description; ?>">
                            <span class="help-block"><?php echo $course_description_err; ?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="menage-courses.php" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>

