<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    // Include config
    require_once 'config.php';

    // Initializing variables
    $name = $course_id = $grade = '';
    $name_err = $course_id_err = $grade_err = '';

    if (isset($_POST['id']) && !empty($_POST['id'])) {
        // Update logic goes here
        $id = $_POST['id'];

        // Validation name
        $input_name = trim($_POST['name']);
        if (empty($input_name)) {
            $name_err = 'Please fill out the student name';
        } elseif (!filter_var($input_name, FILTER_VALIDATE_REGEXP, array("options" => array("regexp"=>"/^[a-zA-Z ]*$/")))) {
            $name_err = 'Please enter valid name';
        } else {
            $name = $input_name;
        }

        // Validation course
        //var_dump($_POST['course']);
        if (empty(trim($_POST['course']))) {
            $course_id_err = 'Please enter course';
        } else {
            $course_id = $_POST['course'];
        }
        //var_dump($course_id_err);die;

        // Validation grade
        if (empty(trim($_POST['grade']))) {
            $grade_err = 'Please enter grade';
        } elseif (!ctype_digit($_POST['grade'])) {
            $grade_err = 'Please enter a numeric grade value';
        } elseif (intval($_POST['grade']) > 6) {
            $grade_err = 'Please enter a numeric grade from 1 to 5';
        } else {
            $grade = $_POST['grade'];
        }

        if (empty($name_err) && empty($course_err) && empty($grade_err)) {
            
            $sql = "UPDATE students SET name=:name, course_id=:course_id, grade=:grade WHERE s_id=:id";

            // Prepare
            if ($stmt = $pdo->prepare($sql)) {

                $stmt->bindParam(":name", $param_name);
                $stmt->bindParam(":course_id", $param_course_id);
                $stmt->bindParam(":grade", $param_grade);
                $stmt->bindParam(":id", $param_id);

                $param_name = $name;
                $param_course_id = $course_id;
                $param_grade = $grade;
                $param_id = $id;

                if($stmt->execute()) {
                    $_SESSION['message'] = 'You have successfully updated a record';
                    header('Location: index.php');
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);

        }
        //unset($pdo);
    } else {
        // Showing input values logic goes here
        if(isset($_GET['id']) && !empty(trim($_GET['id']))) {

            // We get here the url parametar in a variable
            $id = trim($_GET['id']);

            // Sql statement
            //$sql = "SELECT * FROM students WHERE id = :id";
            $sql = "SELECT * FROM students
            INNER JOIN courses ON students.course_id = courses.c_id WHERE students.s_id = :id";

            // Prepare statement
            if ($stmt = $pdo->prepare($sql)) {
                $stmt->bindParam(":id", $param_id);

                // Set param id
                $param_id = $id;

                // Execute
                if ($stmt->execute()) {
                    if ($stmt->rowCount() == 1) {
                        $row = $stmt->fetch(PDO::FETCH_ASSOC);
                        $name = $row['name'];
                        $course = $row['course_name'];
                        $grade = $row['grade'];

                    } else {
                        // There is no valid id param
                        header('Location: error.php');
                        exit();
                    }
                } else {
                    echo 'Something went wrong';
                }
            }
            unset($stmt);
            //unset($pdo);
        } else {
            header('Location: error.php');
            exit();
        }
    }

?>

<?php require_once 'site/header.php'; ?>

<?php $sql = 'SELECT * FROM courses'; ?>

    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">Update Record</h2>
                </div>
                <div class="py-4">
                    <p>Please edit the input values and submit to update the record</p>
                    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
                        <div class="form-group <?php echo (!empty($name_err)) ? 'has-error' : ''; ?>">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control" value="<?php echo $name; ?>">
                            <span class="help-block"><?php echo $name_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($course_id_err)) ? 'has-error' : ''; ?>">
                            <label for="course">Course</label>
                            <select name="course" class="form-control" id="course">
                                <?php if($result = $pdo->query($sql)): ?>
                                    <?php if($result->rowCount() > 0): ?>    
                                        <?php while($row = $result->fetch()): ?>
                                            <option <?= ($course == $row['course_name']) ? 'selected' : ''; ?> value="<?= $row['c_id']; ?>"><?= $row['course_name'] ?></option>
                                        <?php endwhile; ?>
                                    <?php endif; ?>
                                <?php endif; ?>
                                <?php unset($pdo); ?>
                            </select>
                            <span class="help-block"><?php echo $course_id_err; ?></span>
                        </div>
                        <div class="form-group <?php echo (!empty($grade_err)) ? 'has-error' : ''; ?>">
                            <label for="grade">Grade</label>
                            <!-- <input type="text" name="grade" class="form-control" value="<?php //echo $grade; ?>"> -->
                            <select name="grade" class="form-control" id="grade">
                                <option selected value="">Choose a grade</option>
                                <?php for($i = 1; $i < 6; $i++) :?>
                                    <option <?= ($grade == $i) ? 'selected' : ''; ?> value="<?= $i; ?>"><?= $i; ?></option>
                                <?php endfor; ?>
                            </select>
                            <span class="help-block"><?php echo $grade_err; ?></span>
                        </div>
                        <input type="hidden" name="id" value="<?php echo $id; ?>">
                        <button type="submit" class="btn btn-primary">Submit</button>
                        <a href="index.php" class="btn btn-light">Cancel</a>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php require_once 'site/footer.php'; ?>