<?php
    session_start();
    if(!isset($_SESSION['loggedin']) || $_SESSION['loggedin'] !== true) {
        header('location: login.php');
        exit;
    }

    if (isset($_GET['id']) && !empty(trim($_GET['id']))) {

        require_once 'config.php';

        $sql = 'SELECT * FROM courses WHERE c_id = :id';

        if($stmt = $pdo->prepare($sql)) {

            $stmt->bindParam(':id', $param_id);

            $param_id = trim($_GET['id']);

            if($stmt->execute()) {
                if($stmt->rowCount() > 0) {
                    $row = $stmt->fetch(PDO::FETCH_ASSOC);
                    $courseName = $row['course_name'];
                    $courseDescription = $row['course_description'];
                } else {
                    header('Location: error.php');
                } 
            } else {
                echo 'Something went wrong';
            }
            unset($stmt);
            unset($pdo);
        } else {
            header('Location: error.php');
        }

    }

?>

<?php require_once 'site/header.php';  ?>
    <section class="content">
        <div class="row mb-5 mt-5">
            <div class="col-2"></div>
            <div class="col-8">
                <div class="border-bottom mb-4">
                    <h2 class="mb-3">View Course</h2>
                </div>
                <div class="py-4">
                    <div>
                        <p class="font-weight-bold">Name</p>
                        <p><?php echo $courseName; ?></p>
                    </div>
                    <div>
                        <p class="font-weight-bold">Course</p>
                        <p><?php echo $courseDescription; ?></p>
                    </div>
                    <a href="menage-courses.php" class="btn btn-primary">Back</a>
                </div>
            </div>
        </div>
    </section>
<?php require_once 'site/footer.php'; ?>